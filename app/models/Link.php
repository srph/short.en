<?php

class Link extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'links';

	/**
	 * The columns fillable by this model
	 *
	 * @var array
	 */
	protected $fillable = array(
		'user_id',
		'slug',
		'url',
		'created_at',
		'updated_at'
	);

	/** 
	 * Belongs-To Relationship with the User model
	 *
	 * @return 	mixed
	 */
	public function user()
	{
		return $this->belongsTo('User');
	}

	/**
	 * Select respective slug for the link
	 *
	 * @return 	string
	 */
	public static function slug()
	{
		return (Input::has('slug'))
			? Input::get('slug')
			: Str::random(6, Input::get('url'));
	}

	/**
	 * Selects respective user id for the link
	 *
	 * @return 	int
	 */
	public static function getUserId()
	{
		return (Sentry::check())
			? Sentry::getUser()->id
			: null;
	}

	/**
	 * Checks if the requested link exists
	 *
	 * @param 	integer 	$link
	 * @return 	bool
	 */
	public static function exists($link)
	{	
		return (!empty($link))
			? true
			: false;
	}

	/**
	 * Fetches the existing link from a given suer
	 *
	 * @return 	mixed
	 */
	public static function fetch()
	{
		$user_id = self::getUserId();
		$url = Input::get('url');
		$link = Link::where('user_id', '=', $user_id)
					->where('url', '=', $url)
					->first();

		return $link;
	}
}