<?php

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{

		DB::table('users')->delete();
		
		$users = array(

			array(
				'id'			=>	1,
				'email'			=>	'a@a.com',
				'password'		=>	'1234',
				'activated'		=>	true,
				'first_name'	=>	'John',
				'last_name'		=>	'Doe',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime,
				'username'		=>	'username1'
			),

			array(
				'id'			=>	2,
				'email'			=>	'b@b.com',
				'password'		=>	'1234',
				'activated'		=>	true,
				'first_name'	=>	'John',
				'last_name'		=>	'Doe',
				'created_at'	=>	new DateTime,
				'updated_at'	=>	new DateTime,
				'username'		=>	'username2'
			),


		);

		foreach($users as $user) {
			Sentry::createUser($user);
		}
	}

}