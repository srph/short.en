<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	/**
	 * Default layout used by the controller
	 *
	 * @var 	string
	 */
	protected $layout = 'template.master';

	public function getIndex()
	{
		$user = (Sentry::check()) ? User::find(Sentry::getUser()->id) : null;

		$this->layout->title = (Sentry::check()) ? "Your links"	: "Home";

		$this->layout->content = View::make('index')->with('user', $user);
	}

}