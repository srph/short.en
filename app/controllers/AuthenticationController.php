<?php

/**
 * Handles user authentication and registrations
 */

class AuthenticationController extends BaseController {

	/**
	 * Apply filters to all instances of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('guest', array('except' => array('getLogout')));
		$this->beforeFilter('csrf', array('except' => array('getLogout')));
	}

	/**
	 * Authenticate the user
	 *
	 * @return 	Response
	 */
	public function postLogin()
	{
		try {
			$credentials = array(
				'username'	=>	Input::get('username'),
				'password'	=>	Input::get('password')
			);

			$user = Sentry::authenticate($credentials, false);
			Session::flash('success', 'You have been logged in');
			return Response::json(array('status' => true));

			$error = 'An error has occured while logging in';
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			$error = 'Login field is required.';
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			$error = 'Password field is required.';
		} catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
			$error = 'Wrong password, try again.';
		} catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
			$error = 'User was not found.';
		} catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
			$error = 'User is not activated.';
		}

		Session::flash('error', $error);
		return Response::json(array('status' => false, 'error' => $error, 'load' => Input::get('username')));
	}

	/**
	 * Register and authenticate the user
	 *
	 * @return 	Response
	 */
	public function postRegister()
	{
		try {
			$user_created = Sentry::register(array(
				'username'	=>	Input::get('username'),
				'password'	=>	Input::get('password'),
				'email'		=>	Input::get('email')
			));

			if($user_created) {
				Session::flash('sucess', 'You have succesfully created your account!');
				return Response::json(array('status' => true));
			}
		} catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
			$error = 'Login field is required.';
		} catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
			$error = 'Password field is required.';
		} catch (Cartalyst\Sentry\Users\UserExistsException $e) {
			$error = 'User with this login already exists.';
		}

		Session::flash('error', $error);
		return Response::json(array('status' => false));
	}

	public function getLogout()
	{
		if(Sentry::check()) {
			Session::flash('success', 'You have been logged out succesfully!');
			Sentry::logout();
		}

		return Redirect::route('home');
	}

}