<?php

class LinkController extends BaseController {

	/**
	 * Default layout used by the controller
	 *
	 * @var 	string
	 */
	protected $layout = 'template.master';

	/**
	 * Apply filter to all instances of this controller
	 *
	 * @return 	void
	 */
	public function __construct()
	{
		$this->beforeFilter('csrf', array('except' => array('index', 'show', 'destroy')));
		$this->beforeFilter('auth', array('only' => array('index')));
	}

	/**
	 * Display all resources
	 *
	 * @return 	Response
	 */
	public function index()
	{
		$user = User::find(Sentry::getUser()->id);
		$this->layout->title = "Home";
		$this->layout->content = View::make('hello')->with('links', $user->links);
	}

	/**
	 * Store a new resource
	 *
	 * @return 	Response
	 */
	public function store()
	{
		// If link already exists under the user id
		$link = Link::fetch();
		if(Link::exists($link)) {			
			return Response::json(array(
				'status'	=>	true,
				'slug'		=>	$link->slug,
				'url'		=>	$link->url
			));
		}

		// Bind the new link's respective parameters
		$link = new Link(array(
			'user_id'		=>	Link::getUserId(),
			'slug'			=>	Link::slug(),
			'url'			=>	Input::get('url'),
			'created_at'	=>	new DateTime,
			'updated_at'	=>	new DateTime
		));

		// If the new link was stored succ0esfully send a green mark
		if($link->save()) {
			Session::flash('success', 'We have succesfully shortened the link.');
			return Response::json(array(
				'status' 	=>	false,
				'slug'		=>	$link->slug,
				'url'		=>	$link->url,
			));
		}

		Session::flash('error', 'An error has occured.');
		return Response::json(array('status' => false));
	}

	/**
	 * Redirect to the specified resource
	 *
	 * @param 	string 		$slug
	 * @return 	Response
	 */
	public function show($slug)
	{
		$link = Link::where('slug', '=', $slug)->first();
		if(!$link) App::abort('404');
		return Redirect::to($link->url);
	}

	/**
	 * Update a requested resource
	 *
	 * @param 	int 		$id
	 * @return 	Response
	 */
	public function update($id)
	{
		$link = Link::find($id);

		if($link->user !== Sentry::getUser()->id) {
			Session::flash('error', 'The link does not belong to you');
			return Redirect::route('home');
		}

		$link->slug = Input::get('slug');
		$link->updated_at = new DateTime;

		if($link->save()) {
			Session::flash('success', 'Your link has been updated succesfully');
			return Response::json(array(
				'status'	=>	true,
				'slug'		=>	$link->slug
			));
		}

		return Response::json(array('status' => false));
	}

	/**
	 * Delete the requested resource
	 *
	 * @return 	Response
	 */
	public function destroy($id)
	{
		$link = Link::find($id);

		if($link->user->id == Sentry::getUser()) {
			if($link->delete()) {
				Session::flash('success', 'We have succesfully deleted your link');
				return Response::json(array('status' => true));
			}
		}

		Session::flash('error', 'An error has occured');
		return Response::json(array('status' => false));
	}

}