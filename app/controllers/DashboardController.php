<?php

class DashboardController extends BaseController {

	/**
	 * Default layout used by the controller
	 *
	 * @var 	string
	 */
	protected $layout = 'template.master';

	/**
	 * Display all resources
	 *
	 * @return 	Response
	 */
	public function getIndex()
	{
		$this->layout->content =  View::make('hello')
			->with('title', 'Your links');
	}

}