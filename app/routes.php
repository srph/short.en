<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Index */
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@getIndex'));
Route::get('{slug}', array('as' => 'link.redirect', 'uses' => 'LinkController@show'));

/* Authenication */
Route::post('login', array('as' => 'login', 'uses' => 'AuthenticationController@postLogin'));
Route::post('register', array('as' => 'register', 'uses' => 'AuthenticationController@postRegister'));
Route::get('logout', array('as' => 'logout', 'uses' => 'AuthenticationController@getLogout'));

/* Links */
Route::resource('link' ,'LinkController');

/* Debugger */
Route::get('/g/test', function() {
	echo Str::random(6, 'http://facebook.com');

	var_dump(Link::where('user_id', '=', null)
					->where('url', '=', 'http://facebook.com')
					->first()->slug);

	$slug = (Input::has('slug'))
			? Input::get('slug')
			: Str::random(6, 'http://gago.com');

	// Link::create(array(
	// 	'user_id'		=>	null,
	// 	'slug'			=>	$slug,
	// 	'url'			=>	'http://gago.com',
	// 	'created_at'	=>	new DateTime,
	// 	'updated_at'	=>	new DateTime,
	// ));

	// 	$link = new Link(array(
	// 	'user_id'		=>	null,
	// 	'slug'			=>	$slug,
	// 	'url'			=>	'http://gagows.com',
	// 	'created_at'	=>	new DateTime,
	// 	'updated_at'	=>	new DateTime
	// 	));

	// 	if($link->save()) {
	// 		echo '<br> die';
	// 	}


});