@section('content')
	<div class="jumbotron">
		<h1 class="text-center"> {{ Config::get('settings.title') }} your link </h1>

		{{ Form::open(array('route' => 'link.store', 'method' => 'POST', 'id' => 'link')) }}
			{{ Form::token() }}
			{{ Form::text('url', null, array('class' => 'form-control input-lg'))}}
			{{ Form::submit(null, array('hidden' => 'hidden')) }}
		{{ Form::close()}}
	</div>
@stop