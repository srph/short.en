<!DOCTYPE html>
<html lang="en">
<head>
	<title> {{ Config::get('settings.title') }} - {{ $title }} </title>
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/bootstrap.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/stylesheet.css') }}">
	<meta name="token" content="{{ Session::token() }}">
	<meta charset="utf-8">
</head>

<body>
	<div class="container">
		{{-- Navigation --}}
		<nav class="navbar navbar-default" role="navigation">
			{{-- Navigation Header --}}
			<div class="navbar-header">
				{{-- Mobile Display --}}
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
					<span class="sr-only">Toggle Navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<a class="navbar-brand">
					{{ Config::get('settings.title') }}
				</a>
			</div>

			{{-- Navigation Links --}}
			<div class="collapse navbar-collapse" id="#navbar-collapse-1">
				<ul class="nav navbar-nav">
					<li> <a href="{{ URL::route('home') }}"> Home </a> </li>
				</ul>

				{{-- USL --}}
				<ul class="nav navbar-nav navbar-right">
					@if(!Sentry::check())
						<li> <a href="#" data-toggle="modal" data-target="#login-modal"> Login </a> </li>
						<li> <a href="#" data-toggle="modal" data-target="#register-modal"> Register </a> </li>
					@else
						{{-- Header --}}
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<b class="caret"></b>
								Dropdown
							</a>

							<ul class="dropdown-menu">
								<li> <a href="#"> Settings </a> </li>

								<li> <a href="{{ URL::route('logout') }}"> Logout </a> </li>
							</ul>
						</li>
					@endif
				</ul>
			</div>
		</nav>

		{{-- Content --}}
		@include('template/alert')
		@yield('content')
	</div>

	{{-- Scripts --}}
	<script src="{{ URL::asset('js/jquery-1.10.2.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js')}}"></script>
	<noscript> <h2> We require our users to enable Javascript to continue using our service </h2> </noscript>
	@include('template/modals')
	@yield('scripts')
</body>
</html>