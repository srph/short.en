{{-- Login Modal --}}
<div class="modal fade" id="login-modal" tab-index="-1" role="dialog" aria-labelledby="login-modal-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title text-center" id="login-modal-label"> Welcome, User! </h2>
			</div>

			<div class="modal-body">
				{{ Form::open(array('route' => 'login', 'method' => 'POST', 'id' => 'auth', 'data-link' => 'login')) }}
					{{ Form::token() }}

					<div class="form-group">
						<label> Username </label>
						{{ Form::text('username', null, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label> Password </label>
						{{ Form::password('password', array('class' => 'form-control')) }}
					</div>
					{{ Form::submit('Sign in!', array('id' => 'auth', 'class' => 'btn btn-primary')) }}

					{{-- Sign in with API --}}

				{{ Form::close() }}
			</div>			
		</div>
	</div>
</div>
{{-- Login Modal --}}

{{-- Sign Up Modal --}}
<div class="modal fade" id="register-modal" tab-index="-1" role="dialog" aria-labelledby="register-modal-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title text-center" id="register-modal-label"> Create a new account! </h2>
			</div>

			<div class="modal-body">
				{{ Form::open(array('route' => 'register', 'method' => 'POST', 'id' => 'auth', 'data-link' => 'register')) }}
					{{ Form::token() }}

					<div class="form-group">
						<label> Username </label>
						{{ Form::text('username', NULL, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label> Email </label>
						{{ Form::email('email', NULL, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label> Password </label>
						{{ Form::password('password', array('class' => 'form-control')) }}
					</div>

					<hr>

					<div class="form-group">
						<label> First Name </label>
						{{ Form::text('first_name', NULL, array('class' => 'form-control')) }}
					</div>

					<div class="form-group">
						<label> Last Name </label>
						{{ Form::text('last_name', NULL, array('class' => 'form-control')) }}
					</div>
					{{ Form::submit('Sign up!', array('class' => 'btn btn-primary')) }}

					{{-- Sign up with API --}}

				{{ Form::close() }}
			</div>			
		</div>
	</div>
</div>
{{-- Sign Up Modal --}}

{{-- Link Modal --}}
<div class="modal fade" id="link-modal" tab-index="-1" role="dialog" aria-labelledby="link-modal-label" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h2 class="modal-title text-center" id="link-modal-label"> Shortened Link </h2>
			</div>

			<div class="modal-body">
				<small>
					@if(isset($url))
						{{ $url }}
					@endif
				</small>
				{{ Form::text('slug', (isset($slug)) ? $slug : null, array('class' => 'form-control')) }}
			</div>
		</div>
	</div>
</div>
{{-- Link Modal --}}

<script>
	var auth = $('#auth');

	auth.on('submit', function(e) {
		e.preventDefault();
		$this = $(this);

		$.ajax({
			url: "{{ url() }}/" + $this.data('link'),
			type: 'POST',
			beforeSend: function(request) {
				return request.setRequestHeader("X-CSRF-Token", $("meta[name='token']").attr('content'));
			},
			success: function(data) {
				if(data.status == true) {
					console.log('hey');
					location.reload();
				} else {
					console.log(data.error + ' ' + data.error);
				}
			},
			dataType: 'json'
		});

		return false;
	});

	$('#link').on('submit', function(e) {
		e.preventDefault();

		$.ajax({
			url: "{{ url() }}/link",
			type: 'POST',
			success: function(data) {
				if(data.status) {
					// Call Modal
					console.log('success');
					// $('#link-modal').modal();
					window.location.reload();
				}
			},
			dataType: 'json'
		});
	});
</script>