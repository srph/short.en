@if(Session::has('success'))
	<div class="alert alert-success">
		<p>
			{{ Session::get('success') }}
		</p>
	</div>
@elseif(Session::has('warning'))
	<div class="alert alert-warning">
		<p>
			{{ Session::get('warning') }}
		</p>
	</div>
@elseif(Session::has('error'))
	<div class="alert alert-error">
		<p>
			{{ Session::get('error') }}
		</p>
	</div>
@endif